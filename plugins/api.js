export default function ({ $axios }, inject) {
    const axios = $axios.create({
        baseURL: process.env.NUXT_HOST_API
    })

    inject('axios', axios)
}