import Vue from 'vue'

Vue.filter('formatDateTime', (value, format = "DD/MM/YYYY HH:mm", zone = 7) => {
    return moment(value).utcOffset(zone).format(format);
})
Vue.filter('formatDate', (value, format = "DD/MM/YYYY", zone = 7) => {
    return moment(value).utcOffset(zone).format(format);
})
Vue.filter('htmlToText', (value) => {
    return value.replace(/<[^>]*>/g, "").replaceAll("&nbsp;", "");
})
Vue.filter('formatMoney', (money) => {
    return money.toLocaleString("en-US").replace(/,/g, ".") + "đ";
})
