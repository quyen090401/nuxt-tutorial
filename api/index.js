let config = {
    api: process.env.NUXT_HOST_API,

    /*
     *  Without auth
     * */
    TestApi: 'api/users'
};

let api = new Proxy(config, {
    get(target, name) {
        if (name !== "params") {
            return (
                Reflect.get(target, "api").replace(/\/$/, "") +
                Reflect.get(target, name)
            );
        } else {
            return Reflect.get(target, name);
        }
    },
});

api.params = (name, options) => {
    try {
        let endpoint = api[name];
        for (let value in options) {
            if (value && options.hasOwnProperty(value)) {
                endpoint = endpoint.replace(`{${value}}`, options[value]);
            }
        }
        return endpoint;
    } catch (e) {
        console.log(e);
    }
};

export default api;
