export default {
  target: 'static',
  head: {
    title: 'base',
    htmlAttrs: {
      lang: 'vi'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  router: {
    middleware: 'router'
  },
  css: [],
  buildModules: [
    '@nuxtjs/style-resources'
  ],
  styleResources: {
    scss: [
      '~assets/scss/_variables.scss',
      '~assets/scss/mixins/*.scss',
      '~assets/scss/core/*.scss',
      '~assets/scss/**/*.scss'
    ]
  },
  plugins: [
    '~/plugins/axios',
    '~/plugins/api',
    '~plugins/filters'
  ],
  components: true,
  buildModules: [
  ],
  modules: [
    'bootstrap-vue/nuxt',
    '@nuxtjs/axios',
    '@nuxtjs/style-resources'
  ],
  axios: {
    baseURL: process.env.NUXT_HOST_API,
    prefix: '/api',
    credentials: true
  },
  build: {
    extend(config, { loaders }) {
      loaders.scss.additionalData = '@use "sass:map";'
    }
  }
}
