export default {
    data() {
        return {
            entries: null,
            pagination: {
                page: 1,
                length: 12
            }
        }
    },
    methods: {
        setData(entries) {
            this.entries = entries
        },
        getData() {
            return this.entries
        },
        setPagination(pagination) {
            this.pagination = pagination
        },
        getPagination() {
            return this.pagination
        }
    },
}