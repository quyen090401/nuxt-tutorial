export default {
    data() {
        return {
            entry: null,
        }
    },
    methods: {
        setData(entry) {
            this.entry = entry
        },
        getData() {
            return this.entry
        }
    },
}